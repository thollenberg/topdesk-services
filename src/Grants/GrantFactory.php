<?php


namespace TOPdesk\Grants;

use DateTime;
use TOPdesk\Services\Service;
use TOPdesk\Tenants\Tenant;

class GrantFactory
{

    /**
     * @param int $id
     * @param string $publicId
     * @param string $key
     * @param Service $service
     * @param Tenant $tenant
     * @param DateTime $revocationDate
     * @param bool $active
     * @return Grant
     */
    public static function build(
        int $id,
        string $publicId,
        string $key,
        Service $service,
        Tenant $tenant,
        ?DateTime $revocationDate,
        bool $active = false
    ): Grant
    {
        return new Grant([
            'id' => $id,
            'publicId' => $publicId,
            'key' => $key,
            'service' => $service,
            'tenant' => $tenant,
            'revocationDate' => $revocationDate,
            'active' => $active
        ]);
    }

    public static function buildFromArray(array $raw): Grant {
            return new Grant($raw);
    }

}

