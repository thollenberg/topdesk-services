<?php

namespace TOPdesk\Grants;

use DateTime;
use GuzzleHttp\Psr7\Request;
use Spatie\DataTransferObject\DataTransferObject;
use TOPdesk\Tools\GrantTools;

class Grant extends DataTransferObject {
    const ENCRYPT_METHOD = 'AES-256-CBC';

    /**
     * @var int|null
     */
    public $id;

    /**
     * @var string|null
     */
    public $publicId;

    /**
     * @var string|null
     */
    public $key;

    /**
     * @var TOPdesk\Services\Service|null
     */
    public $service;

    /**
     * @var TOPdesk\Tenants\Tenant|null
     */
    public $tenant;

    /**
     * @var DateTime|null
     */
    public $revocationDate;

    /**
     * @var boolean
     */
    public $active;

    public function hasValidKey(): bool {
        if(is_null($this->service)) return false;
        if(is_null($this->tenant)) return false;
        if(is_null($this->tenant->source)) return false;
        $authorization = GrantTools::getAuthorizationFromGrantKey($this, GrantTools::getGrantKeyFromAuthKey($this->key));
        $client = new \GuzzleHttp\Client(['base_uri' => $this->tenant->source]);
        $headers = ['Authorization' => "Basic $authorization"];
        $request = new Request('HEAD', '/tas/api/operators/current', $headers, '');
        $response = $client->send($request);
        if($response->getStatusCode() !== 200)  {
            return false;
        } else {
            return true;
        }
    }

    public function generateKey(string $userName, string $appKey): ?string {
        if(empty($this->publicId) || empty($this->service) || empty($this->tenant)) {
            return null;
        } else {
            return base64_encode(
                $this->publicId . ':' .
                base64_encode(
                    openssl_encrypt(
                        base64_encode("$userName:$appKey"),
                        self::ENCRYPT_METHOD,
                        hash('sha256', $this->service->publicId),
                        0,
                        substr(hash('sha256', $this->tenant->publicId), 0, 16)
                    )
                )
            );
        }
    }
}

