<?php

namespace TOPdesk;

use DateTime;
use DI\Container;
use Dotenv\Dotenv;
use GuzzleHttp\Client;
use PDO;
use PDOException;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use TOPdesk\Grants\Grant;
use TOPdesk\Grants\GrantFactory;
use TOPdesk\Tools\GrantTools;
use TOPdesk\Tools\ServiceDbTools;

class Services {
    const DB_CONNECTION = 'SERVICES_DB_CONNECTION';
    const DB_USER = 'SERVICES_DB_USER';
    const DB_PASSWORD = 'SERVICES_DB_PASSWORD';

    /**
     * @var Container
     */
    private $container;

    /**
     * @var Dotenv
     */
    private $env;

    /**
     * @var PDO
     */
    private $db;

    /**
     * @var bool
     */
    private $initialized = false;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var Grant
     */
    private $grant;

    /**
     * @var Client
     */
    private $client;

    public function __construct(Container $container) {
        $this->container = $container;
        $this->logger = $container->has(LoggerInterface::class) ? $container->get(LoggerInterface::class) : new NullLogger();
        $this->env = $container->has(Dotenv::class) ? $container->get(Dotenv::class) : Dotenv::create(__DIR__);
        $this->env->safeLoad();
        $this->initServiceDb();
    }

    /**
     * @param LoggerInterface $logger
     * @return Services
     */
    public function setLogger(LoggerInterface $logger): Services {
        $this->logger = $logger;
        return $this;
    }

    /**
     * @param PDO $db
     * @return Services
     */
    public function setDb(PDO $db): Services {
        $this->db = $db;
        $this->initialized = true;
        return $this;
    }

    /**
     * @return bool
     */
    public function isInitialized(): bool {
        return $this->initialized;
    }

    /**
     * @param string $key
     * @return string
     */
    public function getAuthFromKey(string $key): string
    {
        $grant = $this->getGrantFromKey($key);
        if (empty($grant) || empty($grant->service) || empty($grant->tenant)) {
            return '';
        } else {
            return GrantTools::getAuthorizationFromGrantKey($grant, GrantTools::getGrantKeyFromAuthKey($key));
        }
    }

    /**
     * @param string $key
     * @return Grant|null
     */
    public function getGrantFromKey(string $key): ?Grant
    {
        // Check for base64 encoded key
        if (!GrantTools::isBase64Encoded($key)) {
            $this->logger->error("No base64 encoded grant key");
            return null;
        }
        $grantId = GrantTools::getGrantIdFromAuthKey($key);
        if (empty($grantId)) {
            $this->logger->error("No grant key", $grantId);
            return null;
        }
        $grant = $this->getGrant($grantId);
        return $grant;
    }

    public function isAllowed(string $grantId, string $serviceId, string $tenantId): bool
    {
        $grant = $this->getGrant($grantId);
        if (empty($grant)) return false;
        if ($grant->service->publicId === $serviceId && $grant->tenant->publicId === $tenantId) {
            return true;
        } else {
            return false;
        }
    }

    public function getGrant(string $publicId): Grant {
        $rawGrant = ServiceDbTools::getGrant($this->db, $publicId);
        $grantModel = null;

        // Grant is not found or service Db is not found
        if(is_null($rawGrant)) {
            $this->logger->error("Grant $publicId is not found");
            return null;
        } else {
            $grantModel = (object) $rawGrant;
        }

        if (!$grantModel->active) {
            $this->logger->error("Grant $grantModel->publicId is not active");
            return null;
        } elseif(!is_null($grantModel->revocationDate) && ($grantModel->revocationDate < time() && $grantModel->revocationDate != 0)) {
            $this->logger->error("Grant $grantModel->publicId is revoked");
            return null;
        }

        $service = ServiceDbTools::getServiceById($this->db, $grantModel->serviceId);
        if(is_null($service)) {
            $this->logger->error("Service $grantModel->serviceId is not found");
        } elseif (!$service->active) {
            $this->logger->error("Service $service->id ($service->description) is not active");
            return null;
        }

        $tenant = ServiceDbTools::getTenantById($this->db, $grantModel->tenantId);
        if(is_null($tenant)) {
            $this->logger->error("Tenant $grantModel->tenantId is not found");
            return null;
        } elseif (!$tenant->active) {
            $this->logger->error("Tenant $tenant->id ($tenant->source) is not active");
            return null;
        }

        $grant = GrantFactory::build(
            $grantModel->id,
            $grantModel->publicId,
            $grantModel->key,
            $service,
            $tenant,
            strtotime($grantModel->revocationDate) < 0 ? null : (new DateTime())->setTimestamp(strtotime($grantModel->revocationDate)),
            $grantModel->active
        );

        if(!$grant->hasValidKey()) {
            $this->logger->error("Grant $grantModel->publicId for $tenant->source is not valid");
            return null;
        }
        return $grant;
    }

    private function initServiceDb() {
        $dbConnection = getenv(self::DB_CONNECTION);
        $dbUser = getenv(self::DB_USER);
        $dbPassword = getenv(self::DB_PASSWORD);
        try {
            $this->db = new PDO($dbConnection, $dbUser, $dbPassword);
            $this->logger->info("Services database is available");
            $this->initialized = true;
        } catch(PDOException $e) {
            $this->logger->error("Services database is unavailable", $e->getTrace());
            $this->db = null;
            $this->initialized = false;
        }
    }
}
