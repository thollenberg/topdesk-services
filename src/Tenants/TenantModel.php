<?php


namespace TOPdesk\Tenants;

use TOPdesk\Components\Model;

class TenantModel extends Model {

    /**
     * @var string|null
     */
    public $source;

    /**
     * @var string|null
     */
    public $description;

    /**
     * @param array $row
     * @return TenantModel|null
     */
    public static function fromRow(?array $row): ?TenantModel {
        return is_null($row) ? null : new self([
            'id' => is_null($row['id']) ? null : (string) $row['id'],
            'publicId' => $row['publicId'],
            'source' => $row['source'],
            'description' => $row['description'],
            'active' => is_null($row['active']) ? null : (boolean) $row['active']
        ]);
    }

    /**
     * @param Tenant $tenant
     * @return TenantModel|null
     */
    public static function fromTenant(Tenant $tenant): ?TenantModel {
        return new self([
            'id' => is_null($tenant->id) ? null : (string) $tenant->id,
            'publicId' => $tenant->publicId,
            'source' => $tenant->source,
            'description' => $tenant->description,
            'active' => $tenant->active
        ]);
    }

}
