<?php

namespace TOPdesk\Tenants;

use Spatie\DataTransferObject\DataTransferObject;
use TOPdesk\Services\Service;
use TOPdesk\Services\ServiceModel;

class Tenant extends DataTransferObject {
    /**
     * @var int|null
     */
    public $id;

    /**
     * @var string|null
     */
    public $publicId;

    /**
     * @var string|null
     */
    public $source;

    /**
     * @var string|null
     */
    public $description;

    /**
     * @var boolean
     */
    public $active;

    /**
     * @param TenantModel|null $model
     * @return Tenant|null
     */
    public static function fromTenantModel(?TenantModel $model): ?Tenant {
        return is_null($model) ? null : new self([
            'id' => empty($model->id) ? null : (int) $model->id,
            'publicId' => empty($model->publicId) ? null : $model->publicId,
            'source' => empty($model->source) ? null : $model->source,
            'description' => empty($model->description) ? null : $model->description,
            'active' => $model->active
        ]);
    }

}
