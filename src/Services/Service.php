<?php


namespace TOPdesk\Services;

use Spatie\DataTransferObject\DataTransferObject;

class Service extends DataTransferObject {
    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $publicId;

    /**
     * @var string|null
     */
    public $description;

    /**
     * @var boolean
     */
    public $active;

    /**
     * @param ServiceModel $model
     * @return Service|null
     */
    public static function fromServiceModel(?ServiceModel $model): ?Service {
        return is_null($model) ? null : new self([
            'id' => empty($model->id) ? null : (int) $model->id,
            'publicId' => empty($model->publicId) ? null : $model->publicId,
            'description' => empty($model->description) ? null : $model->description,
            'active' => $model->active
        ]);
    }
}
