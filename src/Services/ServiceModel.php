<?php


namespace TOPdesk\Services;

use TOPdesk\Components\Model;

class ServiceModel extends Model {

    /**
     * @var string|null
     */
    public $description;

    /**
     * @param array $row
     * @return ServiceModel|null
     */
    public static function fromRow(?array $row): ?ServiceModel {
        return is_null($row) ? null : new self([
            'id' => is_null($row['id']) ? null : (string) $row['id'],
            'publicId' => $row['publicId'],
            'description' => $row['description'],
            'active' => is_null($row['active']) ? null : (boolean) $row['active']
        ]);
    }

    /**
     * @param Service $service
     * @return ServiceModel|null
     */
    public static function fromService(Service $service): ?ServiceModel {
        return new self([
            'id' => is_null($service->id) ? null : (string) $service->id,
            'publicId' => $service->publicId,
            'description' => $service->description,
            'active' => $service->active
        ]);
    }
}
