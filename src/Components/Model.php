<?php

namespace TOPdesk\Components;

use Spatie\DataTransferObject\DataTransferObject;

class Model extends DataTransferObject {
    /**
     * @var string|null
     */
    public $id;

    /**
     * @var string|null
     */
    public $publicId;

    /**
     * @var boolean
     */
    public $active;
}
