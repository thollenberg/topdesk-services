<?php


namespace TOPdesk\Tools\Grants;

use DI\Container;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use TOPdesk\Grants\Grant;
use TOPdesk\Services\Service;
use TOPdesk\Tenants\Tenant;
use TOPdesk\Tools\GrantTools;
use TOPdesk\Tools\ServiceDbTools;

class GrantFactory
{
    /**
     * @var Container
     */
    private $container;

    /**
     * @var PDO
     */
    private $db;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * GrantFactory constructor.
     * @param Container $c
     * @throws \DI\DependencyException
     * @throws \DI\NotFoundException
     */
    public function __construct(Container $c)
    {
        $this->container = $c;
        $this->db = $this->container->has(PDO::class) ? $this->container->get(PDO::class) : null;
        $this->logger = $this->container->has(LoggerInterface::class) ? $this->container->get(LoggerInterface::class) : new NullLogger();
    }

    /**
     * @param int $id
     * @param string $publicId
     * @param string $key
     * @param Service $service
     * @param Tenant $tenant
     * @param DateTime|null $revocationDate
     * @param bool $active
     * @return Grant
     */
    public function fromValues(
        int $id,
        string $publicId,
        string $key,
        Service $service,
        Tenant $tenant,
        ?DateTime $revocationDate,
        bool $active = false
    ): Grant
    {
        return new Grant([
            'id' => $id,
            'publicId' => $publicId,
            'key' => $key,
            'service' => $service,
            'tenant' => $tenant,
            'revocationDate' => $revocationDate,
            'active' => $active
        ]);
    }

    /**
     * @param array $raw
     * @return Grant
     */
    public function fromArray(array $raw): Grant
    {
        return new Grant($raw);
    }

    /**
     * @param string $authKey
     * @return Grant|null
     */
    public function fromAuthKey(string $authKey): ?Grant
    {
        $publicId = GrantTools::getGrantIdFromAuthKey($authKey);
        if (empty($publicId)) {
            $this->logger->error('No valid auth key', [__CLASS__, __FUNCTION__, $authKey]);
            return null;
        }
        return $this->fromPublicId($publicId);
    }

    /**
     * @param string $publicId
     * @return Grant|null
     */
    public function fromPublicId(string $publicId): ?Grant
    {
        if (empty($this->db)) {
            $this->logger->error('Services database not available', [__CLASS__, __FUNCTION__]);
            return null;
        }
        $grant = ServiceDbTools::getGrant($this->db, $publicId);
        if (empty($grant)) {
            $this->logger->error('Services database does not contain grant', [__CLASS__, __FUNCTION__, $publicId]);
        }
        return $grant;
    }
}
