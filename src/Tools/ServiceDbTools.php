<?php

namespace TOPdesk\Tools;

use Exception;
use PDO;
use PDOStatement;
use PDOException;
use TOPdesk\Components\Model;
use TOPdesk\Services\Service;
use TOPdesk\Services\ServiceModel;
use TOPdesk\Tenants\Tenant;
use TOPdesk\Tenants\TenantModel;
use Ramsey\Uuid\Uuid;

class ServiceDbTools {

    /**
     * @param PDO $db
     * @param string $publicId
     * @return array
     */
    public static function getGrant(PDO $db, string $publicId): ?array {
        return self::getEntryByPublicId($db, 'grants', $publicId);
    }

    /**
     * @param PDO $db
     * @param int $id
     * @return array
     */
    public static function getGrantById(PDO $db, int $id): ?array {
        return self::getEntryById($db, 'grants', $id);
    }

    /**
     * @param PDO $db
     * @param string $publicId
     * @return Service|null
     */
    public static function getService(PDO $db, string $publicId): ?Service {
        return Service::fromServiceModel(
            self::getServiceModel($db, $publicId)
        );
    }

    /**
     * @param PDO $db
     * @param string $publicId
     * @return ServiceModel|null
     */
    public static function getServiceModel(PDO $db, string $publicId): ?ServiceModel {
        return ServiceModel::fromRow(
            self::getEntryByPublicId($db, 'services', $publicId)
        );
    }

    /**
     * @param PDO $db
     * @param int $id
     * @return Service|null
     */
    public static function getServiceById(PDO $db, int $id): ?Service {
        return Service::fromServiceModel(
            self::getServiceModelById($db, $id)
        );
    }

    /**
     * @param PDO $db
     * @param int $id
     * @return ServiceModel|null
     */
    public static function getServiceModelById(PDO $db, int $id): ?ServiceModel {
        return ServiceModel::fromRow(
            self::getEntryById($db, 'services', $id)
        );
    }

    /**
     * @param PDO $db
     * @param string $publicId
     * @return Tenant|null
     */
    public static function getTenant(PDO $db, string $publicId): ?Tenant {
        return Tenant::fromTenantModel(
            TenantModel::fromRow(
                self::getEntryByPublicId($db, 'tenants', $publicId)
            )
        );
    }

    /**
     * @param PDO $db
     * @param string $publicId
     * @return TenantModel|null
     */
    public static function getTenantModel(PDO $db, string $publicId): ?TenantModel {
        return TenantModel::fromRow(
            self::getEntryByPublicId($db, 'tenants', $publicId)
        );
    }

    /**
     * @param PDO $db
     * @param int $id
     * @return Tenant|null
     */
    public static function getTenantById(PDO $db, int $id): ?Tenant {
        return Tenant::fromTenantModel(
            TenantModel::fromRow(
                self::getEntryById($db, 'tenants', $id)
            )
        );
    }

    /**
     * @param PDO $db
     * @param int $id
     * @return TenantModel|null
     */
    public static function getTenantModelById(PDO $db, int $id): ?TenantModel {
        return TenantModel::fromRow(
            self::getEntryById($db, 'tenants', $id)
        );
    }

    /**
     * @param PDO $db
     * @param string $table
     * @param int $id
     * @return array
     */
    public static function getEntryById(PDO $db, string $table, int $id): ?array {
        $query = "SELECT * FROM $table WHERE id=:id";
        $result = self::get( $db->prepare($query), array( ':id' => $id));
        return sizeof($result) == 0 ? null : $result[0];
    }

    /**
     * @param PDOStatement $stmt
     * @param array $data
     * @return bool
     */
    public static function put(PDOStatement $stmt, array $data): bool {
        if($stmt->execute($data)) {
            return true;
        } else {
            var_dump($stmt->errorInfo());
            return false;
        };
    }

    /**
     * @param PDO $db
     * @param string $table
     * @param string $publicId
     * @return array|null
     */
    public static function getEntryByPublicId(PDO $db, string $table, string $publicId): ?array  {
        $query = "SELECT * FROM $table WHERE publicId=:publicId";
        $result = self::get($db->prepare($query), array( ':publicId' => $publicId));
        return sizeof($result) == 0 ? null : $result[0];
    }

    /**
     * @param PDOStatement $statement
     * @param array $data
     * @return array
     */
    public static function get(PDOStatement $statement, array $data): array {
        try {
            $statement->execute($data);
            return $statement->fetchAll(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            return array();
        }
    }

    public static function getInsertStatement(string  $table, array $fields): string {
        $sql = "INSERT INTO $table ({{items}}) VALUES ({{parameters}})";
        $items = implode(',', array_keys($fields));
        $parameters = implode(',',
            array_map(
                function($k){ return ':'.$k; },
                array_keys($fields)
            )
        );
        return str_replace('{{items}}', $items, str_replace('{{parameters}}', $parameters, $sql));
    }

    /**
     * @param string $table
     * @param array $fields
     * @return string
     */
    public static function getUpdateStatement(string $table, array $fields): string {
        $sql = "UPDATE $table SET {{set}} WHERE id=:id";
        $set = "";
        foreach ($fields as $key => $value) {
            $set .= $key ==='id' ? '' : "$key=:$key,";
        }
        $set = substr($set,-1) === ',' ? substr($set, 0, -1) : $set;
        return str_replace('{{set}}', $set, $sql);
    }

    /**
     * @param PDO $db
     * @param string $table
     * @param Model $model
     * @return bool
     * @throws Exception
     */
    public static function persistModel(PDO $db, string $table, Model $model) : bool {

        if(empty($model->publicId)) {
            $model->publicId = Uuid::uuid4()->serialize();
        }

        if(empty($model->id)) {
            $data = $model->except('id')->toArray();
            $sql = self::getInsertStatement($table, $data);
        } else {
            $data = $model->toArray();
            $sql = self::getUpdateStatement($table, $data);
        }

        $stmt = $db->prepare($sql);

        return self::put($stmt, $data);
    }

    /**
     * @param PDO $db
     * @param Service $service
     * @return Service|null
     * @throws Exception
     */
    public static function persistService(PDO $db, Service $service): ?Service {
        return Service::fromServiceModel(
            self::persistServiceModel($db,
                ServiceModel::fromService($service)
            )
        );
    }

    /**
     * @param PDO $db
     * @param ServiceModel $model
     * @return ServiceModel|null
     * @throws Exception
     */
    public static function persistServiceModel(PDO $db, ServiceModel $model): ?ServiceModel {
        if(self::persistModel($db, 'services', $model)) {
            return self::getServiceModel($db, $model->publicId);
        } else {
            return null;
        }
    }

    /**
     * @param PDO $db
     * @param TenantModel $model
     * @return TenantModel|null
     * @throws Exception
     */
    public static function persistTenantModel(PDO $db, TenantModel $model): ?TenantModel {
        if(self::persistModel($db, 'tenants', $model)) {
            return self::getTenantModel($db, $model->publicId);
        } else {
            return null;
        }
    }

    /**
     * @param PDO $db
     * @param Tenant $tenant
     * @return Tenant|null
     * @throws Exception
     */
    public static function persistTenant(PDO $db, Tenant $tenant): ?Tenant {
        return Tenant::fromTenantModel(
            self::persistTenantModel($db,
                TenantModel::fromTenant($tenant)
            )
        );
    }

}
