<?php


namespace TOPdesk\Tools;


use TOPdesk\Grants\Grant;

class GrantTools {
    const ENCRYPT_METHOD = 'AES-256-CBC';

    public static function getAuthKey(Grant $grant, string $userName, string $appKey)
    {
        return base64_encode(
            $grant->publicId . ':' .
            base64_encode(
                openssl_encrypt(
                    base64_encode("$userName:$appKey"),
                    self::ENCRYPT_METHOD,
                    hash('sha256', $grant->service->publicId),
                    0,
                    substr(hash('sha256', $grant->tenant->publicId), 0, 16)
                )
            )
        );
    }

    public static function getAuthorizationFromGrantKey(Grant $grant, $grantKey): ?string
    {
        return empty($grantKey) || empty($grant) || empty($grant->service) || empty($grant->tenant) ?
            null :
            openssl_decrypt(
                    base64_decode($grantKey),
                    self::ENCRYPT_METHOD,
                hash('sha256', $grant->service->publicId),
                    0,
                substr(hash('sha256', $grant->tenant->publicId), 0, 16)
            );
    }

    public static function getItemFromAuthKey(string $key, int $itemNr): ?string
    {
        if (self::isBase64Encoded($key)) {
            $rawKey = base64_decode($key);
            $keyItems = explode(':', $rawKey, 2);
            if (sizeof($keyItems) == 2 && ($itemNr == 0 || $itemNr == 1)) {
                return $keyItems[$itemNr];
            }
        }
        return null;
    }

    public static function getGrantIdFromAuthKey($authKey): ?string
    {
        return self::getItemFromAuthKey($authKey, 0);
    }

    public static function getGrantKeyFromAuthKey($authKey): ?string
    {
        return self::getItemFromAuthKey($authKey, 1);
    }


    /**
     * @param string $value
     * @return bool
     */
    public static function isBase64Encoded(string $value): bool
    {
        return (preg_match('%^[a-zA-Z0-9/+]*={0,2}$%', $value));
    }

}
