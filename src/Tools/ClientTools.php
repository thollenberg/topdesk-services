<?php


namespace TOPdesk\Tools;


use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use TOPdesk\Grants\Grant;

class ClientTools
{
    /**
     * @param Grant $grant
     * @return Client|null
     */
    public static function fromGrant(?Grant $grant): ?Client
    {
        if (self::isEligibleGrant($grant) && self::hasValidKey($grant)) {
            return new Client([
                'base_uri' => $grant->tenant->source,
                'headers' => [
                    'Authorization' => 'Basic ' . GrantTools::getAuthorizationFromGrantKey($grant, GrantTools::getGrantKeyFromAuthKey($grant->key))
                ]
            ]);
        } else {
            return null;
        }
    }

    /**
     * @param Grant|null $grant
     * @return bool
     */
    public static function isEligibleGrant(?Grant $grant): bool
    {
        if (
            empty($grant) ||
            empty($grant->key) ||
            empty($grant->service) ||
            empty($grant->tenant) ||
            empty($grant->tenant->source) ||
            !$grant->active
        ) return false;
        return true;
    }

    private static function hasValidKey(Grant $grant): bool
    {
        $authorization = GrantTools::getAuthorizationFromGrantKey($grant, GrantTools::getGrantKeyFromAuthKey($grant->key));
        $client = new Client(['base_uri' => $grant->tenant->source]);
        $headers = ['Authorization' => "Basic $authorization"];
        $request = new Request('HEAD', '/tas/api/operators/current', $headers, '');
        $response = $client->send($request);
        if ($response->getStatusCode() !== 200) {
            return false;
        } else {
            return true;
        }
    }
}
