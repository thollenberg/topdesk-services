<?php


namespace TOPdesk;

use PHPUnit\Framework\TestCase;
use TOPdesk\Services\ServiceModel;
use TOPdesk\Tenants\TenantModel;
use TOPdesk\Tools\ServiceDbTools;

require_once 'Database/DbTestHelper.php';

class ServiceDbToolsTest extends TestCase {

    public function testGetInsertStatement() {
        $insertStmt = ServiceDbTools::getInsertStatement('table', array(
            'publicId' => 'value',
            'serviceId' => 'value'
        ));
        $this->assertEquals('INSERT INTO table (publicId,serviceId) VALUES (:publicId,:serviceId)', $insertStmt);
    }

    public function testGetUpdateStatementWhenIdIsGiven() {
        $stmt = ServiceDbTools::getUpdateStatement('table', array(
            'id' => 'value',
            'publicId' => 'value',
            'serviceId' => 'value'
        ));
        $this->assertEquals('UPDATE table SET publicId=:publicId,serviceId=:serviceId WHERE id=:id', $stmt);
    }

    public function testGetUpdateStatementWhenIdIsNotGiven() {
        $stmt = ServiceDbTools::getUpdateStatement('table', array(
            'publicId' => 'value',
            'serviceId' => 'value'
        ));
        $this->assertEquals('UPDATE table SET publicId=:publicId,serviceId=:serviceId WHERE id=:id', $stmt);
    }

    public function testGetGrantReturnsArrayWhenPublicIdIsKnown() {
        $grant = ServiceDbTools::getGrant(DbTestHelper::getTestDatabase(), DbTestHelper::GRANT_TEST_PUBLIC_ID);
        $this->assertNotNull($grant);
        $this->assertEquals(DbTestHelper::GRANT_TEST_PUBLIC_ID, $grant['publicId']);
    }

    public function testGetGrantReturnsNullWhenPublicIdIsUnknown() {
        $grant = ServiceDbTools::getGrant(DbTestHelper::getTestDatabase(), 'INVALIDPUBLICID');
        $this->assertNull($grant);
    }

    public function testGetGrantReturnsArrayWhenIdIsKnown() {
        $grant = ServiceDbTools::getGrantById(DbTestHelper::getTestDatabase(), DbTestHelper::GRANT_TEST_ID);
        $this->assertNotNull($grant);
        $this->assertEquals(DbTestHelper::GRANT_TEST_ID, $grant['id']);
    }

    public function testGetGrantReturnsNullWhenIdIsUnknown() {
        $grant = ServiceDbTools::getGrantById(DbTestHelper::getTestDatabase(), -1);
        $this->assertNull($grant);
    }

    public function testGetServiceReturnServiceWhenPublicIdIsKnown() {
        $service = ServiceDbTools::getService(DbTestHelper::getTestDatabase(), DbTestHelper::SERVICE_TEST_PUBLIC_ID);
        $this->assertNotNull($service);
        $this->assertEquals(DbTestHelper::SERVICE_TEST_PUBLIC_ID, $service->publicId);
    }

    public function testGetServiceReturnsNullWhenPublicIdIsUnknown() {
        $service = ServiceDbTools::getService(DbTestHelper::getTestDatabase(), 'INVALIDPUBLICID');
        $this->assertNull($service);
    }

    public function testGetServiceReturnsArrayWhenIdIsKnown() {
        $service = ServiceDbTools::getServiceById(DbTestHelper::getTestDatabase(), DbTestHelper::SERVICE_TEST_ID);
        $this->assertNotNull($service);
        $this->assertEquals(DbTestHelper::SERVICE_TEST_ID, $service->id);
    }

    public function testGetServiceReturnsNullWhenIdIsUnknown() {
        $service = ServiceDbTools::getServiceById(DbTestHelper::getTestDatabase(), -1);
        $this->assertNull($service);
    }
    public function testGetTenantReturnsArrayWhenPublicIdIsKnown() {
        $tenant = ServiceDbTools::getTenant(DbTestHelper::getTestDatabase(), DbTestHelper::TENANT_1_TEST_PUBLIC_ID);
        $this->assertNotNull($tenant);
        $this->assertEquals(DbTestHelper::TENANT_1_TEST_PUBLIC_ID, $tenant->publicId);
    }

    public function testGetTenantReturnsNullWhenPublicIdIsUnknown() {
        $tenant = ServiceDbTools::getTenant(DbTestHelper::getTestDatabase(), 'INVALIDPUBLICID');
        $this->assertNull($tenant);
    }

    public function testGetTenantReturnsArrayWhenIdIsKnown() {
        $tenant = ServiceDbTools::getTenantById(DbTestHelper::getTestDatabase(), DbTestHelper::TENANT_1_TEST_ID);
        $this->assertNotNull($tenant);
        $this->assertEquals(DbTestHelper::TENANT_1_TEST_ID, $tenant->id);
    }

    public function testGetTenantReturnsNullWhenIdIsUnknown() {
        $tenant = ServiceDbTools::getTenantById(DbTestHelper::getTestDatabase(), -1);
        $this->assertNull($tenant);
    }

    public function testPersistServiceModelOnNewServiceReturnsNewServiceModel() {
        $serviceModel = new ServiceModel(array('description' => 'Test', 'active' => false));
        $newServiceModel = ServiceDbTools::persistServiceModel(DbTestHelper::getTestDatabase(), $serviceModel);
        $this->assertNotNull($newServiceModel);
    }

    public function testPersistServiceModelOnExistingServiceModelReturnsExistingServiceModel() {
        $db = DbTestHelper::getTestDatabase();
        $serviceModel = ServiceDbTools::getServiceModel($db, DbTestHelper::SERVICE_TEST_PUBLIC_ID);
        $serviceModel->active = false;
        $newServiceModel = ServiceDbTools::persistServiceModel($db, $serviceModel);
        $this->assertEquals(false, $newServiceModel->active);
    }

    public function testPersistTenantModelOnNewTenantReturnsNewServiceModel() {
        $model = new TenantModel(array('source' => 'source', 'description' => 'Test', 'active' => false));
        $newModel = ServiceDbTools::persistTenantModel(DbTestHelper::getTestDatabase(), $model);
        $this->assertNotNull($newModel);
    }

    public function testPersistTenantModelOnExistingTenantModelReturnsExistingTenantModel() {
        $db = DbTestHelper::getTestDatabase();
        $tenantModel = ServiceDbTools::getTenantModel($db, DbTestHelper::TENANT_1_TEST_PUBLIC_ID);
        $tenantModel->active = false;
        $newTenantModel = ServiceDbTools::persistTenantModel($db, $tenantModel);
        $this->assertEquals(false, $newTenantModel->active);
    }

}
