<?php
namespace TOPdesk;


use DI\Container;
use DI\ContainerBuilder;
use Dotenv\Dotenv;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Monolog\Processor\UidProcessor;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;

/**
 * Class ContainerTestHelper
 * @package TOPdesk
 */
class ContainerTestHelper {
    /**
     * @return Container
     */
    public static function getContainer(): Container {
        $builder = new ContainerBuilder();
        $builder->addDefinitions([
            'settings' => [
                'displayErrorDetails' => true, // Should be set to false in production
                'logger' => [
                    'name' => 'test',
                    'path' => isset($_ENV['docker']) ? 'php://stdout' : __DIR__ . '/../logs/app.log',
                    'level' => Logger::DEBUG,
                ],
            ],
            LoggerInterface::class => function (ContainerInterface $c) {
                $settings = $c->get('settings');

                $loggerSettings = $settings['logger'];
                $logger = new Logger($loggerSettings['name']);

                $processor = new UidProcessor();
                $logger->pushProcessor($processor);

                $handler = new StreamHandler($loggerSettings['path'], $loggerSettings['level']);
                $logger->pushHandler($handler);

                return $logger;
            },
            Dotenv::class => function () {
                $env = Dotenv::create(__DIR__ . DIRECTORY_SEPARATOR . '.env.test');
                return $env;
            }
        ]);
        return $builder->build();
    }
}
