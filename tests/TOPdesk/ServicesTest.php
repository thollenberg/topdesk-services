<?php

namespace TOPdesk;

use PHPUnit\Framework\TestCase;

require_once 'Database/DbTestHelper.php';
require_once 'Container/ContainerTestHelper.php';

class ServicesTest extends TestCase {

    public function testInitializedIsFalseWhenDbIsUnreachable() {
        $services = new Services(ContainerTestHelper::getContainer());
        $this->assertEquals(false, $services->isInitialized());
    }

    public function testInitializedIsTrueWhenDbIsPassed() {
        $services = (new Services(ContainerTestHelper::getContainer()))
            ->setDb(DbTestHelper::getTestDatabase());
        $this->assertEquals(true, $services->isInitialized());
    }

    public function testGetGrantReturnsGrantDtoWhenValidPublicIdIsPassed() {
        $services = (new Services(ContainerTestHelper::getContainer()))
            ->setDb(DbTestHelper::getTestDatabase());
        $grant = $services->getGrant(DbTestHelper::GRANT_TEST_PUBLIC_ID);
        $this->assertNotNull($grant);
    }
}
