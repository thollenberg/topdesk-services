<?php

namespace TOPdesk;

use PDO;

class DbTestHelper {
    const GRANT_TEST_ID = 1;
    const GRANT_TEST_PUBLIC_ID = '1d608f36-f4c6-4d29-a300-3a6437c2ee9a';

    const SERVICE_TEST_ID = 1;
    const SERVICE_TEST_PUBLIC_ID = '32d5b583-3693-11e9-83a1-06902e00231f';

    const TENANT_1_TEST_ID = 1;
    const TENANT_1_TEST_PUBLIC_ID = 'e764284f-3692-11e9-83a1-06902e00231f';

    const TENANT_2_TEST_ID = 2;
    const TENANT_2_TEST_PUBLIC_ID = '0f39a8ac-3693-11e9-83a1-06902e00231f';


    /**
     * @codeCoverageIgnore
     */
    public static function getTestDatabase(): PDO {
        $db = new PDO('sqlite::memory:');
        $schema = file_get_contents(__DIR__ . DIRECTORY_SEPARATOR . 'services_schema.sql');
        if(!$db->exec($schema)) var_dump($db->errorInfo());
        return $db;
    }

}
