-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Gegenereerd op: 06 sep 2019 om 11:19
-- Serverversie: 10.2.13-MariaDB
-- PHP-versie: 7.0.28

BEGIN TRANSACTION;

--
-- Database: `allsrvs_services`
--

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `grants`
--

CREATE TABLE `grants` (
  `id`  INTEGER PRIMARY KEY,
  `publicId` varchar(36) NOT NULL,
  `key` varchar(255) NOT NULL,
  `serviceId` int(11) NOT NULL,
  `tenantId` int(11) NOT NULL,
  `type` varchar(20) NOT NULL,
  `expirationDate` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `revocationDate` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `active` tinyint(4) NOT NULL DEFAULT 0
);

--
-- Gegevens worden geëxporteerd voor tabel `grants`
--

INSERT INTO `grants` (`id`, `publicId`, `key`, `serviceId`, `tenantId`, `type`, `expirationDate`, `revocationDate`, `active`) VALUES
(1, '1d608f36-f4c6-4d29-a300-3a6437c2ee9a', 'MWQ2MDhmMzYtZjRjNi00ZDI5LWEzMDAtM2E2NDM3YzJlZTlhOlZqQmhURlpHTUhwdmEwOWlOVk01WmxSaE5YRnJha0pNZFdScVR6ZzNjMnB1TkVGeU1taDZOMEpvWjFCTU5rWkhTVmhRV1VaaVlsaGlkMVp2VUU5V2VWWlhhMmRLVEU4eU1VTmpPVFZYTTFaQ1IzWlpNRUU5UFE9PQ==', 1, 1, 'USAGE', '2020-02-28 23:00:00', '0000-00-00 00:00:00', 1);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `services`
--

CREATE TABLE `services` (
  `id` INTEGER PRIMARY KEY,
  `publicId` varchar(36) NOT NULL,
  `description` varchar(50) NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT 0
);

--
-- Gegevens worden geëxporteerd voor tabel `services`
--

INSERT INTO `services` (`id`, `publicId`, `description`, `active`) VALUES
(1, '32d5b583-3693-11e9-83a1-06902e00231f', 'SolarWinds nCentral Connector', 1);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `tenants`
--

CREATE TABLE `tenants` (
  `id`  INTEGER PRIMARY KEY,
  `publicId` varchar(36) NOT NULL,
  `source` varchar(50) NOT NULL,
  `description` varchar(50) NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT 0
);

--
-- Gegevens worden geëxporteerd voor tabel `tenants`
--

INSERT INTO `tenants` (`id`, `publicId`, `source`, `description`, `active`) VALUES
(1, 'e764284f-3692-11e9-83a1-06902e00231f', 'https://partnerships.topdesk.net', 'TOPdesk Partnerships SaaS environment', 1),
(2, '0f39a8ac-3693-11e9-83a1-06902e00231f', 'https://partnershipsintern.topdesk.net', 'TOPdesk Partnerships internal SaaS playground', 0);

--
-- Indexen voor geëxporteerde tabellen
--

COMMIT TRANSACTION;
